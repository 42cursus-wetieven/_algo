/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algos.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/17 09:22:19 by wetieven          #+#    #+#             */
/*   Updated: 2021/05/19 13:58:06 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
//#include "libft.h"

typedef struct s_cost {
	size_t	checks;
	size_t	swaps;
}	t_cost;

typedef int (*t_srch)(size_t *steps, int *list, const size_t size, const int request);

typedef void (*t_sort)(size_t *steps, int *list, const size_t size);

typedef enum e_alg_idx {
	BIN,
	END
}	t_alg_idx;

typedef struct s_alg {
	enum {
		sort,
		srch,
		unknown,
	}	use;
	
	union {
		t_sort	sort;
		t_srch	srch;
	};
}	t_alg;

typedef struct s_alg_swtch {
	char	*name;
	t_alg	alg;
}	t_alg_swtch;

t_alg	switchboard(const char *requested_algo);
