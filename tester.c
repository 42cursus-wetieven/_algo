#include <stdio.h>
#include <stdlib.h>
#include "algos.h"

void	sort_result(t_alg algo, char *name, int *list, size_t size)
{
	t_cost	data = {.checks = 0, .swaps = 0};
	t_cost *cost;
	*cost = &data;
	size_t	i;
	int		wid = 4; // We should implement a dynamic value here, relative to our numbers size

	ret = algo.sort(&cost, list, size);
	printf("\n%s sorted %zu elements with %zu checks and %zu swaps\n", name, size, cost->checks, cost->swaps);
	// Keep that redundant code or use more parameters ?
}

void	srch_result(t_alg algo, char *name, int request, int *list, size_t size)
{
	int		ret;
	t_cost	data = {.checks = 0, .swaps = 0};
	t_cost *cost;
	*cost = &data;
	size_t	i;
	int		wid = 4;

	ret = algo.srch(&cost, list, size, request);
	if (ret != -1)
		printf("\nValue '%i' found at index %i", request, ret); 
	else
		printf("\nValue '%i' not found", request); 
	printf(" in following list :\n\n");
	printf("IDX : ");
	for (i = 0 ; i < size ; i++) 
		printf("%*i ", wid, (int)i);
	printf("\nVAL : ");
	for (i = 0 ; i < size ; i++) 
		printf("%*i ", wid, list[i]);
	printf("\n\n%s took %zu checks for %zu elements\n\n", name, cost.checks, size);
}

int		main(int ac, char **av)
{
	if (ac > 3)
	{
		t_alg	algo;
		char	*name = av[1];
		size_t	size = ac - 3;
		int		list[size];
		int		request = atoi(av[2]);
		size_t	i;

		for (i = 0 ; i < size ; i++) 
			list[i] = atoi(av[i + 3]);
		algo = switchboard(av[1]);
		switch (algo.use)
		{
		case sort:
			sort_result(algo, name, list, size);
			break;
		case srch:
			srch_result(algo, name, request, list, size);
			break;
		case unknown:
			// Genre, gerer l'erreur
			break;
		}
	}
	return (0);
}
