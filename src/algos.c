#include <string.h>
#include "algos.h"

int	*med_of_3(int **a, int **b, int **c)
{
	if ((**a > **b) ^ (**a > **c))
		return (*a);
	else if ((**b > **a) ^ (**b > **c))
		return (*b);
	else
		return (*c);
}

/*int	mrg_sort(size_t *steps, int *list, const size_t size)
{
	
}*/

void	qck_sort(t_cost **cost, int *list, size_t start, size_t end)
{
	size_t pvt_spot = start;
	size_t scout = start;
	int pivot = list[end];

	if (start == end)
		return ;
	while (scout < end)
	{
		if (list[scout] < pivot)
			pvt_spot = scout;
		(*cost)->checks++;
		scout++;
	}
	list[end] = list[pvt_spot];
	(*cost)->swaps++;
	list[pvt_spot] = pivot;
	(*cost)->swaps++;
	qck_sort(cost, list, start, pvt_spot - 1);
	qck_sort(cost, list, pvt_spot + 1, end);
}

void	med_sort(size_t *steps, int *list, const size_t size)
{
	int *pvt;

	pvt = med_of_3(&list[0], &list[(size - 1)/2], &list[size - 1]);

// meh, c'est probablement l'index de la mediane qu'il me faut en fait.
}

int	bin_srch(t_cost *cost, int *list, const size_t size, const int request)
{
	int	bottom = 0;
	int	top = size - 1;
	int	mid;

	do {
		mid = (top + bottom) / 2;
		if (list[mid] == request)
			return (mid);
		else if (list[mid] < request)
			bottom = mid + 1;
		else
			top = mid - 1;
		cost->checks++;
	}
	while (bottom != top);
	return (-1);
}

t_alg	switchboard(const char *requested_algo)
{
	static t_alg_swtch	algo[] = {
	[BIN] = {.alg = {srch, .srch = &bin_srch}, .name = "binary_search"},
//	[QCK] = {.alg = {sort, .sort = &qck_sort}, .name = "quick_sort"},
//	[MRG] = {.name = "merge_sort", .alg = {sort, .sort = &mrg_sort}},
	[END] = {.name = NULL, .alg = {.use = unknown}}
	};
	t_alg_idx			place = 0;

	while (algo[place].name && strcmp(algo[place].name, requested_algo))
		place++;
	return (algo[place].alg);
}
