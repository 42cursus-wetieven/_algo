# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/10/27 13:50:48 by wetieven          #+#    #+#              #
#    Updated: 2021/05/18 13:39:21 by wetieven         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

# =============== #
# === TARGETS === #
# =============== #

ALGS		=	algos.a
EXEC		=	tester

ODIR		=	obj/
DDIR		=	deps/
SUBDIRS		=	$(ODIR) $(DDIR)

# =============== #
# === SOURCES === #
# =============== #

HDIR		=	include/
SDIR		=	$(shell find src -type d)
LDIR		=	libs

vpath %.h $(HDIR)
vpath %.c $(SDIR)
vpath %.d $(DDIR)
vpath %.o $(ODIR)


# ~~~ Placeholders ~~~ #

INCS		=	$(shell ls $(HDIR))
DEPS		=	$(SRCS:%.c=$(DDIR)%.d)
OBJS		=	$(SRCS:%.c=$(ODIR)%.o)

# ~~~ Main fonctions and additional functions ~~~ #
	# ~~~ sorted by chronological order ~~~ #

SRCS		=	algos.c

ESRC		=	tester.c

# ====================== #
# === COMPILER SETUP === #
# ====================== #

CC			=	gcc
CFLAGS		=	-Wall -Wextra -Werror -O3 -fno-builtin
CINCS		=	$(addprefix -I, $(HDIR))
DFLAGS		=	-MT $@ -MMD -MP -MF $(DDIR)$*.d


# ============= #
# === RULES === #
# ============= #

# ~~~ Default ~~~ #

all			:	$(SUBDIRS) $(ALGS) $(EXEC)

$(SUBDIRS)	:
				mkdir -p $(ODIR)
				mkdir -p $(DDIR)

# ~~~ Compiling  ~~~ #

$(ODIR)%.o	:	%.c $(DDIR)%.d
				$(CC) $(CFLAGS) $(CINCS) $(DFLAGS) -c $< -o $@

$(EXEC)		:	$(ALGS) $(ESRC)
				$(CC) $(CFLAGS) $(CINCS) $(ALGS) $(ESRC) -o $(EXEC)

# ~~~ Lib archiving ~~~ #

$(ALGS)		:	$(OBJS)
				ar rcs $(ALGS) $(OBJS)

# ~~~ Actions ~~~ #

clean		:
				rm -rf $(ODIR)

fclean		:	clean
				rm -rf $(DDIR)
				$(RM) $(ALGS)

re			:	fclean all

.PHONY : all exec bonus clean fclean re

$(DEPS)		:
include $(wildcard $(DEPS))
